# MyStocks
## Description
**MyStocks** is a university project aiming to improve the following skills:
- JavaScript mastery (both on client and server side)
- Document oriented database design (using MongoDB)
- Microservice oriented architecture
- Client-server and real-time communication architecture

The objective is to simulate a stock-options management service that refreshes
stocks data by pulling data from a specialized public API. User should be able
to buy and sell stocks as well as visualize their profit and the stocks value
history (as a graph).

## Credits
This project actively uses the **[Financial Modeling Prep API](https://financialmodelingprep.com/developer/docs/)**, which is totally free and [open-source](https://github.com/antoinevulcain/Financial-Modeling-Prep-API).

## Start with Shell
### Requirements
The script works with any shell interpreter (**sh** is the default one).

System dependencies:
- Docker (19 or more)
- Node (13 or more)
- NPM (6 or more)

NPM global dependencies:
- [nodemon](https://www.npmjs.com/package/nodemon)

### Usage
For development, it is often easier to use plain shell to run services, compared to docker,
as we keep features like hot-reload.

The `start.sh` script launches the backend services (Web API, database and Web UI database manager).
It means that the client application should be run in another shell, like this:

```shell
# In one shell
./start.sh

# In another shell
cd client/
npm run serve
```

Note that the Mongo service is run in a Docker container. If any program is already using the port
27017, it should be stopped before running the script.

## Start with Python
### Requirements
System dependencies:
- Docker (19 or more)
- Node (13 or more)
- NPM (6 or more)
- Python (3.5 or more)

PyPi dependencies:
- [docker](https://pypi.org/project/docker/)
- [python-dotenv](https://pypi.org/project/python-dotenv/)

NPM global dependencies:
- [nodemon](https://www.npmjs.com/package/nodemon)

### Usage
This script share the same goal as the Shell script (see the [previous section](#start-with-shell-script)).
Both scripts start the same back end services so you also need to run the client application
in a separate shell as follow:

```shell
# In one shell
python start.py

# In another shell
cd client/
npm run serve
```

As for the Shell script, the port 27017 needs to be available for the Mongo container
to run properly.

## Start with docker-compose
### Requirements
System dependencies:
- Docker (19 or more)
- Docker Compose (1.23 or more)

### Usage
Run the following command from the base directory:
```shell
docker-compose up
```

## Environment variables
This project strongly relies on environment variables for base configuration.
Every service uses it's own env vars (stored in appropriate files), that are described in the following part.

***Of course the provided files are used for development only. Production ones should never be divulgated in any way.***

### MongoDB
File: `/mongo.env`

| Variable                   | Description   |
| -------------------------- | ------------- |
| MONGO_INITDB_ROOT_USERNAME | Root username |
| MONGO_INITDB_ROOT_PASSWORD | Root password |

### Mongo Express
File: `/mongo-express.env`

| Variable                        | Description             |
| ------------------------------- | ----------------------- |
| ME_CONFIG_OPTIONS_EDITORTHEME   | SQL editor color scheme |
| ME_CONFIG_MONGODB_SERVER        | Mongo server host URL   |
| ME_CONFIG_MONGODB_PORT          | Mongo server port       |
| ME_CONFIG_MONGODB_ADMINUSERNAME | MongoDB admin username  |
| ME_CONFIG_MONGODB_ADMINPASSWORD | MongoDB admin password  |

### MyStocks API
File: `/api.env`

| Variable              | Description                              |
| --------------------- | ---------------------------------------- |
| DB_PORT               | Database server port                     |
| DB_USER               | Database user's username                 |
| DB_PASSWORD           | Database user's password                 |
| FMP_ROOT              | FMP's API root URL                       |
| FMP_PROFILE           | FMP route for a company profile          |
| FMP_PRICES            | FMP route to get stock prices            |
| FMP_HISTORICAL_PRICES | FMP route to get historical stock prices |

### MyStocks Client
File: `/client/.env[.development]`
| Variable         | Description                              |
| ---------------- | ---------------------------------------- |
| VUE_APP_API_HOST | Host URL of the MyStocks Web API         |
| VUE_APP_API_PORT | Server port used by the MyStocks Web API |

## Versions
The following programs have been used for development:
- Node v13.9.0
- NPM 6.13.7
- Docker 19.03.6-ce
- Docker Compose 1.23.2
- Python 3.8.2

## Author
Pierre Duclou - M2 MIAGE SID - 2019/2020

IDMC - Université de Lorraine