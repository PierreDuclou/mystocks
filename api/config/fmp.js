module.exports = {
  root: process.env.FMP_ROOT,
  profile: process.env.FMP_ROOT + process.env.FMP_PROFILE,
  list: process.env.FMP_ROOT + process.env.FMP_LIST,
  historicalPrices: process.env.FMP_ROOT + process.env.FMP_HISTORICAL_PRICES
}