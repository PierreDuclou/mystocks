var mongoose = require('mongoose');

const stockSchema = mongoose.Schema({
    symbol: String,
    company: String,
    price: Number,
    count: Number
});

module.exports = mongoose.model('Stock', stockSchema);