const router = require('express').Router();
const axios = require('axios');

const fmp = require('../config/fmp');
const Stock = require('../models/stock');


router.post('/buy/:symbol', (req, res, next) => {
  axios
    .get(fmp.profile + req.params.symbol)
    .then(response => {
      var stock = new Stock({
        symbol: req.params.symbol,
        company: response.data.profile.companyName,
        price: response.data.profile.price,
        count: req.body.count
      });

      stock.save()
        .then(doc => console.log(doc))
        .catch(err => console.log(err));

      res.status(201).send();
    })
    .catch(error => res.json(error));
});

router.get("/stocks", (req, res, next) => {
  Stock.find().then(doc => {
    var data = [];

    for (var stock of doc) {
      axios
        .get(fmp.profile + stock.symbol)
        .then(response => {
          var currentPrice = response.data.profile.price;
          var difference = stock.price - response.data.profile.price;
          var diffPercentage = difference / stock.price;

          data.push({
            _id: stock._id,
            symbol: stock.symbol,
            company: stock.company,
            price: stock.price,
            count: stock.count,
            currentPrice: currentPrice,
            difference: `${difference} (${diffPercentage.toFixed(2)}%)`
          });

          if (data.length === doc.length) {
            res.json(data);
          }
        })
        .catch(error => console.log(error));
    }

  }).catch(err => console.log(err));
});

module.exports = router;