const router = require('express').Router();
const axios = require('axios');

const fmp = require('../config/fmp');

/**
 * Get the full stock list
 */
router.get('/list', (req, res, next) => {
  axios
    .get(fmp.list)
    .then(response => res.json(response.data.symbolsList))
    .catch(error => res.json(error));
});

/**
 * Get details for the given symbol
 */
router.get('/profile/:symbol', (req, res, next) => {
  axios
    .get(fmp.profile + req.params.symbol)
    .then(response => res.json(response.data.profile))
    .catch(error => res.json(error));
});

/**
 * Get historical prices for the given symbol
 */
router.get('/historical/:symbol', (req, res, next) => {
  axios
    .get(fmp.historicalPrices + req.params.symbol)
    .then(response => {
      var historical = response.data.historical;
      res.json(historical.slice(historical.length - 30));
    })
    .catch(error => res.json(error));
});

module.exports = router;