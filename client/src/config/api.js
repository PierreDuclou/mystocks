const host = process.env.VUE_APP_API_HOST;
const port = process.env.VUE_APP_API_PORT;

export const stocksUrl = `${host}:${port}${process.env.VUE_APP_API_STOCK_LIST}`;
export const historicalUrl = `${host}:${port}${process.env.VUE_APP_API_HISTORICAL}`;
export const profileUrl = `${host}:${port}${process.env.VUE_APP_API_PROFILE}`;
export const buyUrl = `${host}:${port}${process.env.VUE_APP_API_BUY}`;
export const portfolioUrl = `${host}:${port}${process.env.VUE_APP_API_PORTFOLIO}`;