#!/usr/bin/env python

"""MyStocks Services Quick Launch

Description
-----------
    This script aims to make it easier to launch the server side services by
    starting each of them sequentially. The said services are as follows:
    - MongoDB server
    - Mongo Express server (a Web GUI MongoDB manager)
    - ExpressJS Web API

    Both the MongoDB and the MongoExpress services are launched using Docker.

    Note that the VueJS client service has to be started separately.
    That way, API and client logs can be tracked independently.

Dependencies
------------
    docker : https://pypi.org/project/docker/
    python-dotenv : https://pypi.org/project/python-dotenv/
"""

import docker

from os import getenv
from subprocess import call
from dotenv import load_dotenv
from signal import signal, SIGINT


def clean(docker_client: docker.DockerClient):
    """Removes Docker containers and close Docker client

    Parameters:
    -----------
    docker_client : DockerClient
        the Docker client used to remove containers
    """

    to_remove = [getenv("MONGO_CONTAINER"), getenv("MONGOEXPRESS_CONTAINER")]

    for container in docker_client.containers.list(all=True):
        if container.name in to_remove:
            container.remove(force=True)

    docker_client.close()


def main():
    signal(SIGINT, lambda s, f: print("\nInterrupt signal caught."))

    print("Exporting environment variables for development...")
    load_dotenv("api.env")
    load_dotenv("start.env")

    docker_client = docker.from_env()

    try:
        clean(docker_client)

        print("Starting MongoDB service...")
        docker_client.containers.run(
            detach=True,
            name=getenv("MONGO_CONTAINER"),
            environment={
                "MONGO_INITDB_ROOT_USERNAME": getenv("DB_USER"),
                "MONGO_INITDB_ROOT_PASSWORD": getenv("DB_PASSWORD")
            },
            ports={getenv("DB_PORT"): getenv("DB_PORT")},
            image=getenv("MONGO_IMAGE")
        )

        print("Starting Mongo Express service...")
        docker_client.containers.run(
            detach=True,
            name=getenv("MONGOEXPRESS_CONTAINER"),
            environment={
                "ME_CONFIG_OPTIONS_EDITORTHEME": getenv("ME_CONFIG_OPTIONS_EDITORTHEME"),
                "ME_CONFIG_MONGODB_SERVER": getenv("MONGO_CONTAINER"),
                "ME_CONFIG_MONGODB_PORT": getenv("DB_PORT"),
                "ME_CONFIG_MONGODB_ADMINUSERNAME": getenv("DB_USER"),
                "ME_CONFIG_MONGODB_ADMINPASSWORD": getenv("DB_PASSWORD")
            },
            links={getenv("MONGO_CONTAINER"): None},
            ports={getenv("MONGOEXPRESS_PORT"): getenv("MONGOEXPRESS_PORT")},
            image=getenv("MONGOEXPRESS_IMAGE")
        )

    except Exception:
        print("Error: could not connect to Docker socket.")
        exit()

    print("Installing NPM dependencies...")
    call("npm install --silent", shell=True, cwd="api")

    print("\nStarting ExpressJS API...")
    call("npm run dev", shell=True, cwd="api")

    print("Cleaning containers...")
    clean(docker_client)


if __name__ == "__main__":
    main()
