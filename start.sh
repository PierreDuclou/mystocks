#!/usr/bin/env sh

# This script aims to make it easier to launch the server side services by
# starting each of them sequentially. The said services are as follows:
#   - MongoDB server
#   - Mongo Express server (a Web GUI MongoDB manager)
#   - ExpressJS Web API
# Both the MongoDB and the MongoExpress services are launched using Docker.
#
# Note that the VueJS client service has to be started separately.
# That way, API and client logs can be tracked independently.

echo "Exporting environment variables for development..."
export $(cat start.env api.env)
SCRIPTPATH="$(cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P)"

function stop() {
    printf '\nStopping services...\n'
    echo 'Cleaning containers...'
    docker container rm -f $(docker container ls --format='{{.Names}}' | grep 'mystocks')
}

echo "Starting MongoDB service..."
docker container run \
    -d \
    --name $MONGO_CONTAINER \
    -e MONGO_INITDB_ROOT_USERNAME=$DB_USER \
    -e MONGO_INITDB_ROOT_PASSWORD=$DB_PASSWORD \
    -p $DB_PORT:$DB_PORT \
    $MONGO_IMAGE

echo "Starting Mongo Express service..."
docker container run \
    -d \
    --name $MONGOEXPRESS_CONTAINER \
    -e ME_CONFIG_OPTIONS_EDITORTHEME=$ME_CONFIG_OPTIONS_EDITORTHEME \
    -e ME_CONFIG_MONGODB_SERVER=$MONGO_CONTAINER \
    -e ME_CONFIG_MONGODB_PORT=$DB_PORT \
    -e ME_CONFIG_MONGODB_ADMINUSERNAME=$DB_USER \
    -e ME_CONFIG_MONGODB_ADMINPASSWORD=$DB_PASSWORD \
    --link $MONGO_CONTAINER \
    -p $MONGOEXPRESS_PORT:$MONGOEXPRESS_PORT \
    $MONGOEXPRESS_IMAGE

echo "Starting ExpressJS API..."
cd $SCRIPTPATH/api
trap 'stop' SIGINT # executes `stop` function on ^C before killing the process
npm install --silent
npm run dev